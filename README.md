# Gimme CDN

Ce projet contient les sources du projet CDN qui a été utilisé pour rédiger la série d'articles "Créer son CDN de A à Z".

- Partie 1 - Présentation et architecture : https://blog.ziggornif.xyz/post/cdn-part1/
- Partie 2 - Object storage : https://blog.ziggornif.xyz/post/cdn-part2/
- Partie 3 - Core et API : https://blog.ziggornif.xyz/post/cdn-part3/

## Lancement du projet en local

### Object storage Minio

Lancer l'instance Minio à partir du docker-compose présent dans le répertoire docker.

```sh
docker compose up -d
```

### Variables d'environnement

Renommer le fichier `.env.example` en `.env`.

Le fichier utiliser les secrets de dev pré-configurés dans le conteneur Minio.

### Lancement de l'application

```sh
cargo run
```

L'application répond sur le port 8080 par défaut http://localhost:8080/.

Vous pouvez maintenant upload des packages et y accéder via l'UI (ex: http://localhost:8080/gimme/awesome-lib@0.1.0/).