use std::env;

use dotenvy::dotenv;
use gimme::infra::server;

#[derive(PartialEq)]
enum AppEnv {
    Dev,
    Prod,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let app_env = match env::var("APP_ENV") {
        Ok(v) if v == "prod" => AppEnv::Prod,
        _ => AppEnv::Dev,
    };

    if app_env == AppEnv::Dev {
        dotenv().ok();
    }

    env_logger::init();
    let port = env::var("PORT")
        .unwrap_or("8080".to_string())
        .parse::<u16>()
        .expect("Invalid port");

    server::run(port).await
}
