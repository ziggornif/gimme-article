use anyhow::{anyhow, Error, Result};
use async_trait::async_trait;
use futures::future::try_join_all;
use mime_guess::MimeGuess;
use semver::Version;
use std::io::Read;
use std::{io::Cursor, sync::Arc};
use tokio::sync::Mutex;
use zip::ZipArchive;

use super::ports::content_manager_api::ContentManagerAPI;
use super::types::list_objects::ListObjectResult;
use super::{ports::content_storage_spi::ContentStorageAdapter, types::file::File};

pub struct ContentManager {
    content_storage_adapter: Arc<Mutex<dyn ContentStorageAdapter>>,
}

impl ContentManager {
    pub fn new(content_storage_adapter: Arc<Mutex<dyn ContentStorageAdapter>>) -> Self {
        Self {
            content_storage_adapter,
        }
    }
}

#[async_trait]
impl ContentManagerAPI for ContentManager {
    async fn create_package(
        &self,
        name: String,
        version: String,
        file: Vec<u8>,
    ) -> Result<(), Error> {
        Version::parse(&version)
            .map_err(|_| anyhow!("The input version doesn't respect semver format"))?;

        let mut archive = ZipArchive::new(Cursor::new(file))?;
        let dirname = format!("{name}@{version}");

        let mut tasks = Vec::new();

        for i in 0..archive.len() {
            let mut child_file = archive.by_index(i)?;
            let mut file_data: Vec<u8> = Vec::new();
            child_file.read_to_end(&mut file_data)?;

            if !file_data.is_empty() {
                let filename: String = format!("{}/{}", dirname, child_file.name());
                let content_type = MimeGuess::from_path(&filename).first_or_text_plain();

                let adapter = Arc::clone(&self.content_storage_adapter);
                let task = tokio::spawn(async move {
                    adapter
                        .lock()
                        .await
                        .add_object(&filename, &file_data, content_type.as_ref())
                        .await
                });

                tasks.push(task);
            }
        }

        let results = try_join_all(tasks).await?;

        // Check for errors in the results
        for result in results {
            match result {
                Ok(_) => (),
                Err(e) => return Err(e),
            }
        }

        Ok(())
    }

    async fn get_file(
        &self,
        package_name: String,
        version: String,
        file_name: String,
    ) -> Result<File, Error> {
        Version::parse(&version)
            .map_err(|_| anyhow!("The input version doesn't respect semver format"))?;
        let path = format!("{}@{}{}", package_name, version, file_name);
        let file: File = self
            .content_storage_adapter
            .lock()
            .await
            .get_object(path)
            .await?;
        Ok(file)
    }

    async fn get_files(
        &self,
        package_name: String,
        version: String,
        child_dir: Option<String>,
    ) -> Result<ListObjectResult, Error> {
        let path = if let Some(child_dir) = &child_dir {
            format!("{}@{}{}", package_name, version, child_dir)
        } else {
            format!("{}@{}/", package_name, version)
        };

        let result = self
            .content_storage_adapter
            .lock()
            .await
            .list_objects(path)
            .await?;

        Ok(result)
    }
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;
    use tokio::sync::Mutex;

    use anyhow::{Error, Result};
    use regex::Regex;

    use super::ContentManager;
    use crate::domain::ports::{
        content_manager_api::ContentManagerAPI, content_storage_spi::ContentStorageAdapter,
        stubs::inmem_storage::InMemStorage,
    };

    #[tokio::test]
    async fn non_semver_error() -> Result<(), Error> {
        let adapter = InMemStorage::new();
        let cm = ContentManager::new(Arc::new(Mutex::new(adapter)));
        assert!(cm
            .get_file(
                "awesome-lib".to_string(),
                "qwe".to_string(),
                "/awesome-lib.min.js".to_string(),
            )
            .await
            .is_err());
        Ok(())
    }

    #[tokio::test]
    async fn should_get_object() -> Result<(), Error> {
        let mut adapter = InMemStorage::new();
        adapter
            .add_object(
                "awesome-lib@1.0.0/awesome-lib.min.js",
                &mut "console.log('Hello world !!!!');".as_bytes(),
                "application/javascript",
            )
            .await?;
        let cm = ContentManager::new(Arc::new(Mutex::new(adapter)));
        let file = cm
            .get_file(
                "awesome-lib".to_string(),
                "1.0.0".to_string(),
                "/awesome-lib.min.js".to_string(),
            )
            .await?;
        let re = Regex::new(r"^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{1,3})?Z$")?;
        assert!(re.is_match(file.last_modified.as_str()));
        Ok(())
    }
}
