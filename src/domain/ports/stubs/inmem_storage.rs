use crate::domain::{
    ports::content_storage_spi::ContentStorageAdapter,
    types::{
        file::File,
        list_objects::{ListObjectResult, Object},
    },
};
use anyhow::{anyhow, Error};
use async_trait::async_trait;
use chrono::{DateTime, SecondsFormat, Utc};

pub struct InMemStorage {
    bucket: Vec<File>,
}

impl InMemStorage {
    pub fn new() -> Self {
        Self { bucket: vec![] }
    }
}

impl Default for InMemStorage {
    fn default() -> Self {
        Self::new()
    }
}

#[async_trait]
impl ContentStorageAdapter for InMemStorage {
    async fn list_objects(&self, _parent_object: String) -> Result<ListObjectResult, Error> {
        let mut result = ListObjectResult::new();
        result.objects.extend(self.bucket.iter().map(|obj| Object {
            name: obj.name.clone(),
            last_modified: obj.last_modified.clone(),
            size: obj.size,
        }));

        Ok(result)
    }

    async fn add_object(
        &mut self,
        filename: &str,
        object: &[u8],
        content_type: &str,
    ) -> Result<(), Error> {
        let now: DateTime<Utc> = Utc::now();

        self.bucket.push(File {
            name: filename.to_string(),
            binary: object.to_vec(),
            last_modified: now.to_rfc3339_opts(SecondsFormat::Secs, true),
            size: object.len(),
            content_type: content_type.to_string(),
        });
        Ok(())
    }

    async fn get_object(&self, filename: String) -> Result<File, Error> {
        Ok(self
            .bucket
            .iter()
            .find(|obj| obj.name == filename)
            .ok_or(anyhow!("Object not found"))?
            .clone())
    }

    async fn delete_objects(&mut self, filename: String) -> Result<(), Error> {
        if let Some(pos) = self.bucket.iter().position(|obj| obj.name == filename) {
            self.bucket.remove(pos);
        }

        Ok(())
    }
}
