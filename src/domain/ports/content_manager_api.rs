use anyhow::{Error, Result};
use async_trait::async_trait;

use crate::domain::types::{file::File, list_objects::ListObjectResult};

#[async_trait]
pub trait ContentManagerAPI: Send + Sync {
    async fn create_package(
        &self,
        name: String,
        version: String,
        file: Vec<u8>,
    ) -> Result<(), Error>;

    async fn get_file(
        &self,
        package_name: String,
        version: String,
        file_name: String,
    ) -> Result<File, Error>;

    async fn get_files(
        &self,
        package_name: String,
        version: String,
        child_dir: Option<String>,
    ) -> Result<ListObjectResult, Error>;
}
