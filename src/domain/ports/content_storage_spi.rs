use anyhow::Error;
use async_trait::async_trait;

use crate::domain::types::{file::File, list_objects::ListObjectResult};

#[async_trait]
pub trait ContentStorageAdapter: Send + Sync {
    async fn list_objects(&self, parent_object: String) -> Result<ListObjectResult, Error>;
    async fn add_object(
        &mut self,
        filename: &str,
        object: &[u8],
        content_type: &str,
    ) -> Result<(), Error>;
    async fn get_object(&self, filename: String) -> Result<File, Error>;
    async fn delete_objects(&mut self, filename: String) -> Result<(), Error>;
}
