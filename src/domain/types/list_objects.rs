use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct Directory {
    pub name: String,
}

#[derive(Debug, Serialize)]
pub struct Object {
    pub name: String,
    pub last_modified: String,
    pub size: usize,
}

#[derive(Debug, Serialize)]
pub struct ListObjectResult {
    pub objects: Vec<Object>,
    pub directories: Vec<Directory>,
}

impl ListObjectResult {
    pub fn new() -> Self {
        Self {
            objects: vec![],
            directories: vec![],
        }
    }
}

impl Default for ListObjectResult {
    fn default() -> Self {
        Self::new()
    }
}
