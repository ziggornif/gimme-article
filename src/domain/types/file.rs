#[derive(Debug, Clone)]
pub struct File {
    pub name: String,
    pub binary: Vec<u8>,
    pub last_modified: String,
    pub size: usize,
    pub content_type: String,
}
