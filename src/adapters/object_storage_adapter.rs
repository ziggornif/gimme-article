use anyhow::{Error, Result};
use async_trait::async_trait;
use chrono::{DateTime, SecondsFormat, Utc};
use rusoto_core::{credential::StaticProvider, HttpClient, Region};
use rusoto_s3::S3;
use rusoto_s3::{
    DeleteObjectRequest, GetObjectRequest, ListObjectsV2Request, PutObjectRequest, S3Client,
};
use std::io::Read;
use tokio::task;

use crate::domain::{
    ports::content_storage_spi::ContentStorageAdapter,
    types::{
        file::File,
        list_objects::{Directory, ListObjectResult, Object},
    },
};

pub struct ObjectStorageAdapter {
    client: S3Client,
    bucket_name: String,
}

impl ObjectStorageAdapter {
    pub fn new(
        bucket_name: String,
        region: String,
        endpoint: String,
        access_key: String,
        secret_key: String,
    ) -> Result<Self, Error> {
        let provider = StaticProvider::new_minimal(access_key, secret_key);

        let region = Region::Custom {
            name: region,
            endpoint,
        };
        let client = S3Client::new_with(HttpClient::new()?, provider, region);

        Ok(Self {
            client,
            bucket_name,
        })
    }
}

#[async_trait]
impl ContentStorageAdapter for ObjectStorageAdapter {
    async fn list_objects(&self, parent_object: String) -> Result<ListObjectResult, Error> {
        let request = ListObjectsV2Request {
            bucket: self.bucket_name.clone(),
            prefix: Some(parent_object),
            delimiter: Some("/".to_string()),
            ..Default::default()
        };

        let result = self.client.list_objects_v2(request).await?;

        let mut response = ListObjectResult::new();

        if let Some(contents) = result.contents {
            response
                .objects
                .extend(contents.into_iter().map(|obj| Object {
                    name: obj.key.unwrap_or_default(),
                    last_modified: obj.last_modified.unwrap_or_default(),
                    size: obj.size.unwrap_or_default() as usize,
                }));
        }

        if let Some(common_prefixes) = result.common_prefixes {
            response
                .directories
                .extend(common_prefixes.into_iter().map(|prefix| Directory {
                    name: prefix.prefix.unwrap_or_default(),
                }));
        }

        Ok(response)
    }

    async fn add_object(
        &mut self,
        filename: &str,
        object: &[u8],
        content_type: &str,
    ) -> Result<(), Error> {
        let request = PutObjectRequest {
            bucket: self.bucket_name.clone(),
            key: filename.to_owned(),
            body: Some(object.to_owned().into()),
            content_type: Some(content_type.to_owned()),
            ..Default::default()
        };

        self.client.put_object(request).await?;

        Ok(())
    }

    async fn get_object(&self, filename: String) -> Result<File, Error> {
        let request = GetObjectRequest {
            bucket: self.bucket_name.clone(),
            key: filename.clone(),
            ..Default::default()
        };

        let result = self.client.get_object(request).await?;
        let buffer = task::spawn_blocking(|| {
            let mut buffer = Vec::new();
            result
                .body
                .unwrap()
                .into_blocking_read()
                .read_to_end(&mut buffer)?;
            Ok::<Vec<u8>, Error>(buffer)
        })
        .await??;

        let content_type = result
            .content_type
            .unwrap_or_else(|| "text/plain".to_string());

        let size = result.content_length.unwrap_or(buffer.len() as i64) as usize;
        let last_modified = if let Some(last_modif) = result.last_modified {
            let parsed_date = DateTime::parse_from_rfc2822(&last_modif)?;
            // to return with ISO 8601 yyyy-mm-ddThh:mm:ssZ format
            parsed_date.to_rfc3339_opts(SecondsFormat::Secs, true)
        } else {
            let now: DateTime<Utc> = Utc::now();
            now.to_rfc3339_opts(SecondsFormat::Secs, true)
        };

        let file = File {
            name: filename,
            binary: buffer,
            last_modified,
            size,
            content_type,
        };

        Ok(file)
    }

    async fn delete_objects(&mut self, filename: String) -> Result<(), Error> {
        let request = DeleteObjectRequest {
            bucket: self.bucket_name.clone(),
            key: filename,
            ..Default::default()
        };

        self.client.delete_object(request).await?;

        Ok(())
    }
}

// #[cfg(test)]
// mod tests {
//     use super::ObjectStorageAdapter;
//     use crate::domain::ports::content_storage_spi::ContentStorageAdapter;

//     use anyhow::{Error, Result};
//     use dotenvy::dotenv;
//     use regex::Regex;

//     pub fn init_bucket() -> Result<ObjectStorageAdapter, Error> {
//         dotenv().ok();
//         let bucket_name = dotenvy::var("BUCKET").unwrap();
//         let region = dotenvy::var("REGION").unwrap();
//         let endpoint = dotenvy::var("ENDPOINT").unwrap();
//         let access_key = dotenvy::var("ACCESS_KEY").unwrap();
//         let secret_key = dotenvy::var("SECRET_KEY").unwrap();
//         let osm = ObjectStorageAdapter::new(bucket_name, region, endpoint, access_key, secret_key)?;
//         Ok(osm)
//     }

//     #[tokio::test]
//     async fn should_create_object() -> Result<(), Error> {
//         let mut osm = init_bucket()?;
//         osm.delete_objects("test_unit.txt".to_string()).await?;
//         let object = "I'm a unit test file".as_bytes();
//         osm.add_object("test_unit.txt", object, "text/plain")
//             .await?;
//         Ok(())
//     }

//     #[tokio::test]
//     async fn should_list_bucket_objects() -> Result<(), Error> {
//         let mut osm = init_bucket()?;
//         osm.delete_objects("test_unit.txt".to_string()).await?;
//         let object = "I'm a unit test file".as_bytes();
//         osm.add_object("test_unit.txt", object, "text/plain")
//             .await?;
//         let result = osm.list_objects("".to_string()).await?;
//         let re = Regex::new(r"^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{1,3})?Z$")?;
//         assert_eq!("test_unit.txt".to_owned(), result.objects[0].name);
//         assert!(re.is_match(result.objects[0].last_modified.as_str()));
//         Ok(())
//     }

//     #[tokio::test]
//     async fn should_get_object() -> Result<(), Error> {
//         let mut osm = init_bucket()?;
//         osm.delete_objects("test_unit.txt".to_string()).await?;
//         let object = "I'm a unit test file".as_bytes();
//         osm.add_object("test_unit.txt", object, "text/plain")
//             .await?;
//         let res = osm.get_object("test_unit.txt".to_string()).await?;
//         assert_eq!(
//             String::from_utf8(res.binary)?,
//             "I'm a unit test file".to_owned()
//         );
//         Ok(())
//     }
// }
