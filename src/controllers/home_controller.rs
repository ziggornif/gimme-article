use actix_web::{get, web, HttpResponse, Responder};
use tera::{Context, Tera};

#[get("/")]
pub async fn home(tera: web::Data<Tera>) -> impl Responder {
    let template = tera
        .render("home.html", &Context::new())
        .expect("Fail to render package view");
    HttpResponse::Ok().body(template)
}
