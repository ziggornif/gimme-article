use actix_multipart::form::{tempfile::TempFile, text::Text, MultipartForm};
use actix_web::{get, post, web, HttpResponse, Responder, HttpRequest};
use log::debug;
use std::{
    fs,
    path::{Path, PathBuf},
    time::Instant,
};
use tera::{Context, Tera};

use crate::infra::state::State;

#[derive(MultipartForm)]
struct UploadForm {
    name: Text<String>,
    version: Text<String>,
    file: TempFile,
}

fn is_file(file: &str) -> bool {
    let path = Path::new(file);
    path.extension().is_some()
}

fn token_match(req:  HttpRequest, secret: &str) -> bool {
    match req.headers().get("Token") {
        Some(h_token) => {
            h_token.to_str().unwrap() == secret
        },
        None => false
    }
}

#[post("/packages")]
async fn upload_package(
    payload: MultipartForm<UploadForm>,
    req: HttpRequest,
    data: web::Data<State>,
) -> impl Responder {
    if !token_match(req, &data.secret) {
        return HttpResponse::Unauthorized().into()
    }

    let start = Instant::now();
    let content_type = payload.file.content_type.as_ref().unwrap();

    if content_type.to_string() != *"application/zip" {
        return HttpResponse::BadRequest().into();
    }

    let file_data = fs::read(payload.file.file.path()).unwrap();

    match data
        .content_manager
        .create_package(
            payload.name.to_owned(),
            payload.version.to_owned(),
            file_data,
        )
        .await
    {
        Ok(()) => {
            let duration = start.elapsed();
            debug!("Time elapsed in upload_file() is: {:?}", duration);
            HttpResponse::Created().body("".to_string())
        }
        // TODO: improve errors here
        Err(e) => HttpResponse::InternalServerError().body(e.to_string()),
    }
}

async fn get_file(
    data: web::Data<State>,
    package_name: String,
    version: String,
    filename: String,
) -> HttpResponse {
    match data
        .content_manager
        .get_file(package_name, version, filename)
        .await
    {
        Ok(file) => HttpResponse::Ok()
            .insert_header(("Content-Type", file.content_type))
            .body(file.binary),
        // TODO: improve errors here
        Err(e) => HttpResponse::InternalServerError().body(e.to_string()),
    }
}

async fn get_html_package(
    data: web::Data<State>,
    tera: web::Data<Tera>,
    package_name: String,
    version: String,
    filename: Option<String>,
) -> HttpResponse {
    match data
        .content_manager
        .get_files(package_name.clone(), version.clone(), filename.clone())
        .await
    {
        Ok(files) => {
            let mut context = Context::new();
            context.insert("package_name", &package_name);
            context.insert("package_version", &version);
            context.insert("files", &files.objects);
            context.insert("directories", &files.directories);

            if let Some(filename) = filename {
                if filename != "/" {
                    let original_path = format!("{}@{}{}", package_name, version, filename);
                    let path_buf = PathBuf::from(original_path);
                    let back_dir = format!("{}/", path_buf.parent().unwrap().to_str().unwrap());
                    context.insert("back_dir", &back_dir);
                }
            }

            let template = tera
                .render("package.html", &context)
                .expect("Fail to render package view");
            HttpResponse::Ok().body(template)
        }
        // TODO: improve errors here
        Err(e) => HttpResponse::InternalServerError().body(e.to_string()),
    }
}

#[get("/gimme/{package}@{version}{file:.*}")]
pub async fn gimme(
    data: web::Data<State>,
    tera: web::Data<Tera>,
    path: web::Path<(String, String, Option<String>)>,
) -> impl Responder {
    let (package_name, version, filename) = path.into_inner();

    if let Some(filename) = filename.clone() {
        if is_file(&filename) {
            return get_file(data, package_name, version, filename).await;
        }
    }
    get_html_package(data, tera, package_name, version, filename).await
}
