use std::env;
use anyhow::{Error, Result};
use std::sync::Arc;
use tokio::sync::Mutex;

use actix_web::web::Data;

use crate::{
    adapters::object_storage_adapter::ObjectStorageAdapter,
    domain::{content_manager::ContentManager, ports::content_manager_api::ContentManagerAPI},
};

pub struct State {
    pub content_manager: Arc<dyn ContentManagerAPI>,
    pub secret: String
}

pub fn bootstrap() -> Result<Data<State>, Error> {
    let bucket_name = env::var("BUCKET").expect("expected bucket name");
    let region = env::var("REGION").expect("expected object storage region");
    let endpoint = env::var("ENDPOINT").expect("expected object storage endpoint");
    let access_key = env::var("ACCESS_KEY").expect("expected object storage access key");
    let secret_key = env::var("SECRET_KEY").expect("expected object storage secret key");
    let app_secret = dotenvy::var("GIMME_SECRET").expect("expected application secret key");

    let storage_adapter =
        ObjectStorageAdapter::new(bucket_name, region, endpoint, access_key, secret_key)?;
    let content_manager = ContentManager::new(Arc::new(Mutex::new(storage_adapter)));

    Ok(Data::new(State {
        content_manager: Arc::new(content_manager),
        secret: app_secret,
    }))
}
