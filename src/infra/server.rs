use actix_cors::Cors;
use actix_web::{web, App, HttpServer};
use lazy_static::lazy_static;
use log::info;
use tera::Tera;

use crate::{
    controllers::{home_controller::home, package_controller},
    infra::state::bootstrap,
};

lazy_static! {
    pub static ref TEMPLATES: Tera = {
        let mut tera = Tera::default();
        tera.add_raw_template("package.html", include_str!("../templates/package.html"))
            .expect("Expected template");
        tera.add_raw_template("home.html", include_str!("../templates/home.html"))
            .expect("Expected template");
        tera.autoescape_on(vec![".html"]);
        tera
    };
}

pub async fn run(port: u16) -> Result<(), std::io::Error> {
    let server = HttpServer::new(move || {
        let cors = Cors::permissive();
        App::new()
            .wrap(cors)
            .app_data(web::Data::new(TEMPLATES.clone()))
            .app_data(bootstrap().unwrap())
            .service(home)
            .service(package_controller::upload_package)
            .service(package_controller::gimme)
    })
    .bind(("0.0.0.0", port))?
    .run();

    info!("Application running on http://localhost:{}", port);

    server.await
}
